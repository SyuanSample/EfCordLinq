﻿using System.Text.Json;
using DomainModel.Model.Point;
using RepositoryLayer.Domain.Org;
using RepositoryLayer.Domain.PointCenter;

namespace ServiceLayerTest.MemberGroup;

public partial class QueryMemberGroupTest
{
    const            string       baseCompanyName = "測試公司";
    const            string       baseCompanyCode = "BaseCp";
    private readonly List<Member> _members        = InitMember();

    private static List<OrganizationSet> InitOrg()
        => new List<OrganizationSet>()
        {
            new()
            {
                Id         = Guid.NewGuid(), Status = true, Code = $"baseCompanyCode01", Name = $"baseCompanyName01"
              , WalletMode = 4
            }
          , new()
            {
                Id         = Guid.NewGuid(), Status = true, Code = $"baseCompanyCode02", Name = $"baseCompanyName02"
              , WalletMode = 4
            }
            /*  , new()
                {
                    Id = Guid.NewGuid(), Status = true, Code = $"baseCompanyCode03", Name = $"baseCompanyName03"
                  , WalletMode = 4
                }
              , new()
                {
                    Id = Guid.NewGuid(), Status = true, Code = $"baseCompanyCode04", Name = $"baseCompanyName04"
                  , WalletMode = 4
                }*/
        };

    private static List<Member> InitMember()
    {
        var          orgData   = InitOrg();
        const string sys       = "systemCode";
        const string webId     = "WebID";
        const string account   = "Account";
        const int    maxMember = 35;

        var result = new List<Member>();

        foreach (var org in orgData!.Select
                 (
                     (r
                    , i) => new {Row = r, Index = i}
                 ))
        {
            for (var i = 0
                 ; i < maxMember
                 ; i++)
            {
                var lastSeq = (i / 10).ToString()
                                      .PadLeft(2, '0');

                var accountSeq = i.ToString()
                                  .PadLeft(4, '0');

                var id = 0 == org.Index
                             ? i
                             : i + maxMember * org.Index;

                var member = new Member()
                {
                    Id       = id + 1
                  , Company  = org.Row.Code, SystemCode = sys + lastSeq, WebId = webId + lastSeq
                  , Account  = account + accountSeq, Name = account + accountSeq, StopBalance = -1, OpenServerId = "All"
                  , Currency = "TWD", Language = "zh-tw", Fn1 = "0", Fn2 = "0", Fn3 = "0", Fn4 = "0", Fn5 = "0"
                  , Online   = false,
                };

                result.Add(member);
            }
        }

        return result;
    }

    private List<MemberWallet> InitMemberWallets()
    {
        var result = _members.Select
                             (
                                 m =>
                                     new MemberWallet
                                     {
                                         MemberId = m.Id, IsSingleWalletLock = (m.Id / 2 == 0)
                                     }
                             )
                             .ToList();

        return result;
    }
}