﻿using Moq;
using Newtonsoft.Json;
using RepositoryLayer.Interface;
using ServiceLayer.Services;
using JsonConverter = System.Text.Json.Serialization.JsonConverter;

namespace ServiceLayerTest.MemberGroup;

public partial class QueryMemberGroupTest
{
    private Mock<ISingleMemberRepo> mMember;
    private Mock<IOrganizationRepo> mOrg;

    private MemberGroupService _service;

    [SetUp]
    public void Setup()
    {
        mMember = new Mock<ISingleMemberRepo>();
        mOrg    = new Mock<IOrganizationRepo>();

        mOrg.Setup(o => o.QueryOrgInfo(It.IsAny<int>()))
            .ReturnsAsync(InitOrg);

        mMember.Setup(m => m.QueryMember(It.IsAny<List<string>>()))
               .ReturnsAsync(_members.AsQueryable);

        mMember.Setup(m => m.QueryMemberWallet(It.IsAny<List<long>>()))
               .ReturnsAsync(InitMemberWallets);

        _service = new MemberGroupService(mOrg.Object, mMember.Object);
    }

    [Test]
    public void ResultTest()
    {

        var data = _service.QueryMemberGroup().Result;

        var str = JsonConvert.SerializeObject(data);
        Console.WriteLine(str);
        Assert.Pass();
    }
}