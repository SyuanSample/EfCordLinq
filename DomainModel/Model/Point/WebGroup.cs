﻿namespace DomainModel.Model.Point;

/// <summary>
///     站台群組
/// </summary>
public class WebGroup
{
    /// <summary>
    ///     公司
    /// </summary>
    public string Company { get; set; }

    /// <summary>
    ///     系統
    /// </summary>
    public string System { get; set; }

    /// <summary>
    ///     站台
    /// </summary>
    public string WebId { get; set; }

    /// <summary>
    ///     統計
    /// </summary>
    public int Count { get; set; }

    /// <summary>
    ///     會員錢包
    /// </summary>
    public List<MemberWallet> Data { get; set; }
}