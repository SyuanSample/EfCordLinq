﻿namespace DomainModel.Model.Point;

/// <summary>
///     會員錢包
/// </summary>
public class MemberWallet
{
    /// <summary>
    ///     會員ID
    /// </summary>
    public long MemberId { get; set; }

    /// <summary>
    ///     鎖定模式
    /// </summary>
    public bool? IsSingleWalletLock { get; set; }
}