﻿using DomainModel.Model.Point;
using Microsoft.EntityFrameworkCore;
using RepositoryLayer.Context;
using RepositoryLayer.Domain.PointCenter;
using RepositoryLayer.Interface;

namespace RepositoryLayer.Repository;

public class SingleMemberRepo : ISingleMemberRepo
{
    private readonly PointCenterContext _pc;

    public SingleMemberRepo(PointCenterContext pc)
    {
        _pc = pc;
    }

    /// <summary>
    ///     取得特定組織的會員ID
    /// </summary>
    /// <param name="org"></param>
    /// <returns></returns>
    public async Task<List<long>> QueryOrgMemberIds(List<string> org)
    {
        var data = await QueryMember(org);

        var result = await data.Select(x => x.Id)
                               .ToListAsync();

        return result;
    }

    /// <summary>
    ///     回傳會員的錢包狀態
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public async Task<List<MemberWallet>> QueryMemberWallet(List<long> id)
    {
        var result = await _pc.Wallets
                              .Where(w => id.Contains(w.MemberId))
                              .Select
                              (
                                  w => new MemberWallet()
                                  {
                                      MemberId = w.MemberId, IsSingleWalletLock = w.IsSingleWalletLock
                                  }
                              )
                              .ToListAsync();

        return result;
    }

    /// <summary>
    ///     取得特定組織的會員資訊
    /// </summary>
    /// <param name="org"></param>
    /// <param name="id"></param>
    /// <returns></returns>
    public async Task<IQueryable<Member>> QueryMember(List<string> org)
    {
        var result = _pc.Members
                        .AsNoTracking()
                        .Where(m => org.Contains(m.Company));

        return result;
    }
}