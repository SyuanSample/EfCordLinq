﻿using Microsoft.EntityFrameworkCore;
using RepositoryLayer.Context;
using RepositoryLayer.Domain.Org;
using RepositoryLayer.Interface;

namespace RepositoryLayer.Repository;

public class OrganizationRepo : IOrganizationRepo
{
    private readonly OrgContext _org;

    public OrganizationRepo(OrgContext org)
    {
        _org = org;
    }

    /// <summary>
    ///     取得特定錢包模式的公司清單
    /// </summary>
    /// <param name="mode"></param>
    /// <returns></returns>
    public async Task<List<OrganizationSet>> QueryOrgInfo(int mode)
    {
        var result = await _org.OrganizationSets
                         .AsNoTracking()
                         .Where(os => os.WalletMode == mode)
                         .ToListAsync();

        return result;
    }
}