﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using RepositoryLayer.Domain.PointCenter;

namespace RepositoryLayer.Context
{
    public partial class PointCenterContext : DbContext
    {
        public PointCenterContext()
        {
        }

        public PointCenterContext(DbContextOptions<PointCenterContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Member> Members { get; set; } = null!;
        public virtual DbSet<Wallet> Wallets { get; set; } = null!;

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Member>(entity =>
            {
                entity.ToTable("Member");

                entity.HasComment("會員");

                entity.HasIndex(e => new { e.Company, e.LobbyToken }, "IX_CompanyAndToken");

                entity.HasIndex(e => new { e.Company, e.SystemCode, e.WebId, e.Account }, "IX_Member")
                    .IsUnique();

                entity.HasIndex(e => e.LobbyToken, "IX_Token");

                entity.HasIndex(e => new { e.Online, e.Status }, "IndexMember_Online");

                entity.Property(e => e.Id).HasComment("會員Id");

                entity.Property(e => e.Account)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasComment("對應的帳號(Club_Ename)");

                entity.Property(e => e.Company)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasComment("公司");

                entity.Property(e => e.CreateDatetime).HasColumnType("datetime");

                entity.Property(e => e.Currency)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .HasComment("幣別");

                entity.Property(e => e.CurrencyRate)
                    .HasColumnType("money")
                    .HasComment("匯率");

                entity.Property(e => e.DeviceType)
                    .HasMaxLength(20)
                    .HasComment("會員進入的裝置類型(站台端判斷)、H1是紀錄官網所提供的資料、其餘為LobbyEntrance紀錄。");

                entity.Property(e => e.FirstLoginTime)
                    .HasColumnType("datetime")
                    .HasComment("會員首次登入「遊戲」的時間");

                entity.Property(e => e.Fn1)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("FN1")
                    .HasComment("總監Id");

                entity.Property(e => e.Fn2)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("FN2")
                    .HasComment("大股東Id");

                entity.Property(e => e.Fn3)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("FN3")
                    .HasComment("股東Id");

                entity.Property(e => e.Fn4)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("FN4")
                    .HasComment("總代理Id");

                entity.Property(e => e.Fn5)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("FN5")
                    .HasComment("代理Id");

                entity.Property(e => e.GroupList)
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Ip)
                    .HasMaxLength(256)
                    .IsUnicode(false)
                    .HasColumnName("IP");

                entity.Property(e => e.Language)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasComment("語系");

                entity.Property(e => e.LastLoginTime)
                    .HasColumnType("datetime")
                    .HasComment("會員最後一次登入「遊戲」的時間");

                entity.Property(e => e.LobbyToken)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Name)
                    .HasMaxLength(20)
                    .HasComment("會員名稱(Club_Cname)");

                entity.Property(e => e.Online).HasComment("是否在線，1 在線、0 未在線");

                entity.Property(e => e.OpenServerId)
                    .HasMaxLength(512)
                    .IsUnicode(false)
                    .HasComment("開放桌別");

                entity.Property(e => e.Status).HasComment("0:帳號凍結 1:帳號正常");

                entity.Property(e => e.StopBalance).HasComment("停利額度");

                entity.Property(e => e.SystemCode)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasComment("子系統");

                entity.Property(e => e.UpdateDatetime).HasColumnType("datetime");

                entity.Property(e => e.UserAgent)
                    .HasMaxLength(350)
                    .HasComment("會員最後登入的 UserAgent 紀錄");

                entity.Property(e => e.WebId)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasComment("網站");

                entity.Property(e => e.WebMode)
                    .HasDefaultValueSql("((0))")
                    .HasComment("會員進入的站台， 0 未定義(預設值)， 1 綠板， 2 藍板");
            });

            modelBuilder.Entity<Wallet>(entity =>
            {
                entity.HasKey(e => e.MemberId);

                entity.ToTable("Wallet");

                entity.HasComment("錢包");

                entity.HasIndex(e => e.LobbyKickType, "IX_Wallet");

                entity.Property(e => e.MemberId)
                    .ValueGeneratedNever()
                    .HasComment("會員Id");

                entity.Property(e => e.Balance)
                    .HasColumnType("money")
                    .HasComment("目前的點數");

                entity.Property(e => e.GameToken)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IsTest).HasDefaultValueSql("((0))");

                entity.Property(e => e.LobbyKickType).HasDefaultValueSql("((0))");

                entity.Property(e => e.SessionNo)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");
            });

            modelBuilder.HasSequence("RXNumber")
                .HasMin(1)
                .HasMax(99999999)
                .IsCyclic();

            modelBuilder.HasSequence("TXNumber")
                .HasMin(1)
                .HasMax(99999999)
                .IsCyclic();

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
