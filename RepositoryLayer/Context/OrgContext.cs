﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using RepositoryLayer.Domain.Org;

namespace RepositoryLayer.Context
{
    public partial class OrgContext : DbContext
    {
        public OrgContext()
        {
        }

        public OrgContext(DbContextOptions<OrgContext> options)
            : base(options)
        {
        }

        public virtual DbSet<OrganizationSet> OrganizationSets { get; set; } = null!;

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<OrganizationSet>(entity =>
            {
                entity.ToTable("OrganizationSet");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.ClientSecret).HasMaxLength(128);

                entity.Property(e => e.Code).HasMaxLength(64);

                entity.Property(e => e.DesIv)
                    .HasMaxLength(8)
                    .HasColumnName("DesIV");

                entity.Property(e => e.DesKey).HasMaxLength(8);

                entity.Property(e => e.GameUrl)
                    .HasMaxLength(512)
                    .HasColumnName("GameURL");

                entity.Property(e => e.IsTest).HasDefaultValueSql("((0))");

                entity.Property(e => e.Name).HasMaxLength(128);

                entity.Property(e => e.Note).HasMaxLength(512);

                entity.Property(e => e.WalletMode).HasComment("錢包模式(1:點數儲值, 2:全額轉帳, 3:H1), 未來有異動請至FlagMap(FlagMap=OrgWalletMode)");

                entity.Property(e => e.WalletModeCallBackUrl)
                    .HasMaxLength(256)
                    .IsUnicode(false)
                    .HasColumnName("WalletModeCallBackURL")
                    .HasComment("錢包模式回呼URL(點數儲值沒有回呼URL，其他有)");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
