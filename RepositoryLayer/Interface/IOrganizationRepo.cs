﻿using RepositoryLayer.Domain.Org;

namespace RepositoryLayer.Interface;

public interface IOrganizationRepo
{
    /// <summary>
    ///     取得特定錢包模式的公司清單
    /// </summary>
    /// <param name="mode"></param>
    /// <returns></returns>
    public Task<List<OrganizationSet>> QueryOrgInfo(int mode);
}