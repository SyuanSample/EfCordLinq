﻿using DomainModel.Model.Point;
using RepositoryLayer.Domain.PointCenter;

namespace RepositoryLayer.Interface;

public interface ISingleMemberRepo
{
    /// <summary>
    ///     取得特定組織的會員ID
    /// </summary>
    /// <param name="org"></param>
    /// <returns></returns>
    public Task<List<long>>  QueryOrgMemberIds(List<string> org);
    
    /// <summary>
    ///     回傳會員的錢包狀態
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    Task<List<MemberWallet>> QueryMemberWallet(List<long> id);

    /// <summary>
    ///     取得特定組織的會員資訊
    /// </summary>
    /// <param name="org"></param>
    /// <param name="id"></param>
    /// <returns></returns>
    Task<IQueryable<Member>> QueryMember(List<string> org);
}