﻿using System;
using System.Collections.Generic;

namespace RepositoryLayer.Domain.PointCenter
{
    /// <summary>
    /// 會員
    /// </summary>
    public partial class Member
    {
        /// <summary>
        /// 會員Id
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 公司
        /// </summary>
        public string Company { get; set; } = null!;
        /// <summary>
        /// 子系統
        /// </summary>
        public string SystemCode { get; set; } = null!;
        /// <summary>
        /// 網站
        /// </summary>
        public string WebId { get; set; } = null!;
        /// <summary>
        /// 對應的帳號(Club_Ename)
        /// </summary>
        public string Account { get; set; } = null!;
        /// <summary>
        /// 會員名稱(Club_Cname)
        /// </summary>
        public string Name { get; set; } = null!;
        /// <summary>
        /// 停利額度
        /// </summary>
        public int StopBalance { get; set; }
        /// <summary>
        /// 開放桌別
        /// </summary>
        public string OpenServerId { get; set; } = null!;
        /// <summary>
        /// 幣別
        /// </summary>
        public string Currency { get; set; } = null!;
        /// <summary>
        /// 匯率
        /// </summary>
        public decimal? CurrencyRate { get; set; }
        /// <summary>
        /// 語系
        /// </summary>
        public string Language { get; set; } = null!;
        /// <summary>
        /// 總監Id
        /// </summary>
        public string Fn1 { get; set; } = null!;
        /// <summary>
        /// 大股東Id
        /// </summary>
        public string Fn2 { get; set; } = null!;
        /// <summary>
        /// 股東Id
        /// </summary>
        public string Fn3 { get; set; } = null!;
        /// <summary>
        /// 總代理Id
        /// </summary>
        public string Fn4 { get; set; } = null!;
        /// <summary>
        /// 代理Id
        /// </summary>
        public string Fn5 { get; set; } = null!;
        /// <summary>
        /// 是否在線，1 在線、0 未在線
        /// </summary>
        public bool Online { get; set; }
        public string? GroupList { get; set; }
        public string? LobbyToken { get; set; }
        /// <summary>
        /// 0:帳號凍結 1:帳號正常
        /// </summary>
        public short? Status { get; set; }
        public DateTime? CreateDatetime { get; set; }
        public string? Ip { get; set; }
        public DateTime? UpdateDatetime { get; set; }
        /// <summary>
        /// 會員進入的裝置類型(站台端判斷)、H1是紀錄官網所提供的資料、其餘為LobbyEntrance紀錄。
        /// </summary>
        public string? DeviceType { get; set; }
        /// <summary>
        /// 會員最後登入的 UserAgent 紀錄
        /// </summary>
        public string? UserAgent { get; set; }
        /// <summary>
        /// 會員進入的站台， 0 未定義(預設值)， 1 綠板， 2 藍板
        /// </summary>
        public short? WebMode { get; set; }
        /// <summary>
        /// 會員首次登入「遊戲」的時間
        /// </summary>
        public DateTime? FirstLoginTime { get; set; }
        /// <summary>
        /// 會員最後一次登入「遊戲」的時間
        /// </summary>
        public DateTime? LastLoginTime { get; set; }
    }
}
