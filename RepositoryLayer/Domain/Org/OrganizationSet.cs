﻿using System;
using System.Collections.Generic;

namespace RepositoryLayer.Domain.Org
{
    public partial class OrganizationSet
    {
        public Guid Id { get; set; }
        public string? Code { get; set; }
        public string? Name { get; set; }
        public bool Status { get; set; }
        public string? ClientSecret { get; set; }
        public string? DesKey { get; set; }
        public string? DesIv { get; set; }
        public string? GameUrl { get; set; }
        public string? Note { get; set; }
        public bool? IsTest { get; set; }
        public Guid? GeneralAgentId { get; set; }
        /// <summary>
        /// 錢包模式(1:點數儲值, 2:全額轉帳, 3:H1), 未來有異動請至FlagMap(FlagMap=OrgWalletMode)
        /// </summary>
        public short WalletMode { get; set; }
        /// <summary>
        /// 錢包模式回呼URL(點數儲值沒有回呼URL，其他有)
        /// </summary>
        public string? WalletModeCallBackUrl { get; set; }
    }
}
