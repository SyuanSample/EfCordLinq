﻿using DomainModel.Model.Point;

namespace ServiceLayer.Interface;

public interface IMemberGroupService
{
    /// <summary>
    ///     取得會員群組資訊
    /// </summary>
    public Task<List<WebGroup>> QueryMemberGroup();
}