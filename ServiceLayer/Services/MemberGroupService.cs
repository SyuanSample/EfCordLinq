﻿using System.Text.Json;
using System.Text.Json.Serialization;
using DomainModel.Const;
using DomainModel.Model.Point;
using Microsoft.EntityFrameworkCore;
using RepositoryLayer.Interface;
using ServiceLayer.Interface;

namespace ServiceLayer.Services;

public class MemberGroupService : IMemberGroupService
{
    private readonly IOrganizationRepo _org;
    private readonly ISingleMemberRepo _member;

    public MemberGroupService(IOrganizationRepo org
                            , ISingleMemberRepo member)
    {
        _org    = org;
        _member = member;
    }

    /// <summary>
    ///     取得會員群組資訊
    /// </summary>
    public async Task<List<WebGroup>> QueryMemberGroup()
    {
        var orgInfo = await _org.QueryOrgInfo(WalletMode.SingleWallet);

        var orgCode = orgInfo.Select(o => o.Code)
                             .ToList();

        var members = await _member.QueryMember(orgCode);

        var memberId = members.Select(m => m.Id)
                              .ToList();

        var wallets = await  _member.QueryMemberWallet(memberId);

        var webGroup = members.GroupBy
                              (
                                  m =>
                                      new
                                      {
                                          m.Company, m.SystemCode, m.WebId
                                      }
                              )
                              .Select
                              (
                                  g =>
                                      new WebGroup()
                                      {
                                          Company = g.Key.Company, System = g.Key.SystemCode, WebId = g.Key.WebId
                                        , Count   = g.Count(), Data = g.SelectMany
                                                                       (
                                                                           wg => wallets.Where(w => w.MemberId == wg.Id)
                                                                          ,
                                                                           (m
                                                                          , w) => new MemberWallet
                                                                           {
                                                                               MemberId           = w.MemberId
                                                                             , IsSingleWalletLock = w.IsSingleWalletLock
                                                                           }
                                                                       )
                                                                       .ToList()
                                      }
                              )
                              .ToList();


        return webGroup;
    }
}